FROM python:latest
COPY . /Django
WORKDIR /Django
RUN pip install -r requirements.txt
COPY . ./
EXPOSE 8080
ENTRYPOINT ["python3", "manage.py"]
